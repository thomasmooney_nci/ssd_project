class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :title
      t.string :director
      t.string :country
      t.string :genre
      t.text :description, precision: 8, scale: 2
      t.string :image_url
      t.decimal :price

      t.timestamps null: false
    end
  end
end
