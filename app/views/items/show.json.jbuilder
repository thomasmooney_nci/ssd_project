json.extract! @item, :id, :title, :director, :country, :genre, :description, :image_url, :price, :created_at, :updated_at
